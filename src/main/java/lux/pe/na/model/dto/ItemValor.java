package lux.pe.na.model.dto;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class ItemValor implements Serializable {

  private static final long serialVersionUID = 1L;

  private Integer codigo;
  private String descripcion;
}
