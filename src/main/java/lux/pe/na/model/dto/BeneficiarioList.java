package lux.pe.na.model.dto;

import lombok.*;

import java.io.Serializable;
import java.sql.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BeneficiarioList implements Serializable {

  private static final long serialVersionUID = 1L;

  private Integer idBeneficiario;
  private Integer idSolicitud;
  private Integer numeroBeneficiarioDispositivo;
  private Integer codigoTipoBeneficiario;
  private String nombres;
  private String apellidoPaterno;
  private String apellidoMaterno;
  private Integer codigoSexo;
  private Integer codigoParentesco;
  private Integer codigoTipoDocumento;
  private String numeroDocumento;
  private Date fechaNacimiento;
  private Double porcentajeBeneficiario;
}
