package lux.pe.na.model.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class TablaMaestra {

  private Integer id;
  private Integer codigoCampo;
  private String valor;
}
