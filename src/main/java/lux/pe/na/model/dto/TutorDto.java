package lux.pe.na.model.dto;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class TutorDto implements Serializable {

  private static final long serialVersionUID = 1L;

  private String nombres;
  private String apellidoPaterno;
  private String apellidoMaterno;
  private ItemValor sexo;
  private ItemValor parentesco;
  private ItemValor tipoDocumento;
  private String numeroDocumento;
  private Date fechaNacimiento;
}