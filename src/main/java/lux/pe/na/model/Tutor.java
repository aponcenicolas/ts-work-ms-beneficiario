package lux.pe.na.model;


import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name = "Tutor",schema = "Solicitud")
public class Tutor implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IdTutor")
    private Integer idTutor;

    @Column(name = "IdSolicitud")
    private Integer idSolicitud;

    @Column(name = "Nombres", length = 50)
    private String nombres;

    @Column(name = "ApellidoPaterno", length = 40)
    private String apellidoPaterno;

    @Column(name = "ApellidoMaterno", length = 40)
    private String apellidoMaterno;

    @Column(name = "CodigoSexo")
    private Integer codigoSexo;

    @Column(name = "CodigoParentesco")
    private Integer codigoParentesco;

    @Column(name = "CodigoTipoDocumento")
    private Integer codigoTipoDocumento;

    @Column(name = "NumeroDocumento", length = 12)
    private String numeroDocumento;

    @Column(name = "FechaNacimiento")
    private Date fechaNacimiento;

    @Column(name = "FechaCreacion")
    private Timestamp fechaCreacion;

    @Column(name = "UsuarioCreacion", length = 20)
    private String usuarioCreacion;

    @Column(name = "FechaModificacion")
    private Timestamp fechaModificacion;

    @Column(name = "UsuarioModificacion", length = 20)
    private String usuarioModificacion;
}
