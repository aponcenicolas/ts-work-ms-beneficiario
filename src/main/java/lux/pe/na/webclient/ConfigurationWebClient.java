package lux.pe.na.webclient;

import lux.pe.na.model.dto.TablaMaestra;
import reactor.core.publisher.Mono;

public interface ConfigurationWebClient {

  Mono<TablaMaestra> masterTableByIdAndFielCode(Integer id, Integer fielCode);
}
