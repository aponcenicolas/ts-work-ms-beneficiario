package lux.pe.na.webclient.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class WebClientApiConfig {

  @Value("${api.support.url}") // URL PENDIENTE
  private String apiSupportConfigurationUrl;

  @Bean
  public WebClient configurationWebClient(WebClient.Builder webClientBuilder) {
    return webClientBuilder
        .baseUrl(apiSupportConfigurationUrl)
        .build();
  }
}
