package lux.pe.na.webclient.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Getter
public class ConfigurationProperties {

  @Value("${api.support.url}") // URL PENDIENTE
  private String url;

  @Value("${api.support.subscription-key}") // URL PENDIENTE
  private String ocpApimSubscriptionKey;
}
