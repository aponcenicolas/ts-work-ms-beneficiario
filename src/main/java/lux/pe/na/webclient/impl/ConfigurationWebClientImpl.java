package lux.pe.na.webclient.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lux.pe.na.model.dto.TablaMaestra;
import lux.pe.na.webclient.ConfigurationWebClient;
import lux.pe.na.webclient.config.ConfigurationProperties;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Repository;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;


@Repository
@Slf4j
@RequiredArgsConstructor
public class ConfigurationWebClientImpl implements ConfigurationWebClient {

  private final WebClient webClient;
  private final ConfigurationProperties configurationProperties;


  @Override
  public Mono<TablaMaestra> masterTableByIdAndFielCode(Integer id, Integer fielCode) {
    return webClient
        .get()
        .uri("/localhost")
        .headers(httpHeaders -> httpHeaders.setContentType(MediaType.APPLICATION_JSON))
        .header("Ocp-Apim-Subscription-Key", configurationProperties.getOcpApimSubscriptionKey())
        .retrieve()
        .bodyToMono(TablaMaestra.class);
  }
}
