package lux.pe.na.business.impl;

import lombok.AllArgsConstructor;
import lux.pe.na.business.BeneficiaryService;
import lux.pe.na.expose.request.BeneficiaryRequest;
import lux.pe.na.expose.response.BeneficiaryResponse;
import lux.pe.na.mappers.BeneficiaryMapper;
import lux.pe.na.mappers.TutorMapper;
import lux.pe.na.model.Beneficiario;
import lux.pe.na.model.Tutor;
import lux.pe.na.model.dto.*;
import lux.pe.na.repository.BeneficiaryRepository;
import lux.pe.na.repository.TutorRepository;
import lux.pe.na.webclient.ConfigurationWebClient;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class BeneficiaryServiceImpl implements BeneficiaryService {

  private final TutorRepository tutorRepository;
  private final BeneficiaryRepository beneficiaryRepository;
  private final ConfigurationWebClient configurationWebClient;

  @Override
  public Mono<List<BeneficiaryResponse>> getBeneficiary(Integer multipleRequestId) {

    List<BeneficiarioList> beneficiaries = beneficiaryRepository.beneficiaryList(multipleRequestId);
    List<TutorList> tutors = tutorRepository.tutorList(multipleRequestId);

    List<BeneficiaryResponse> beneficiaryResponseList = beneficiaries.stream()
        .map(beneficiary -> {

          Optional<TutorList> tutorOptional = tutors.stream()
              .filter(tutor -> tutor.getIdSolicitud()
                  .equals(beneficiary.getIdSolicitud()))
              .findFirst();

          TutorDto tutorDto = null;

          if (tutorOptional.isPresent()) {
            tutorDto = TutorDto.builder()
                .nombres(tutorOptional.get().getNombres())
                .apellidoPaterno(tutorOptional.get().getApellidoPaterno())
                .apellidoMaterno(tutorOptional.get().getApellidoMaterno())
                .fechaNacimiento(tutorOptional.get().getFechaNacimiento())
                .sexo(ItemValor.builder()
                    .codigo(tutorOptional.get().getCodigoSexo())
                    .descripcion(null)
                    .build())
                .parentesco(ItemValor.builder()
                    .codigo(tutorOptional.get().getCodigoParentesco())
                    .descripcion(null)
                    .build())
                .tipoDocumento(ItemValor.builder()
                    .codigo(tutorOptional.get().getCodigoTipoDocumento())
                    .descripcion(configurationWebClient.masterTableByIdAndFielCode(multipleRequestId,
                            tutorOptional.get().getCodigoTipoDocumento())
                        .map(TablaMaestra::getValor).block())
                    .build())
                .numeroDocumento(tutorOptional.get().getNumeroDocumento())
                .build();
          }

          String descriptionBeneficiary = configurationWebClient.masterTableByIdAndFielCode(multipleRequestId,
                  beneficiary.getCodigoTipoBeneficiario())
              .map(TablaMaestra::getValor).block();

          return BeneficiaryResponse.builder()
              .idBeneficiario(beneficiary.getIdBeneficiario())
              .idSolicitud(beneficiary.getIdSolicitud())
              .tipoBeneficiario(ItemValor.builder()
                  .codigo(beneficiary.getCodigoTipoBeneficiario())
                  .descripcion(descriptionBeneficiary)
                  .build())
              .nombres(beneficiary.getNombres())
              .apellidoPaterno(beneficiary.getApellidoPaterno())
              .apellidoMaterno(beneficiary.getApellidoMaterno())
              .sexo(ItemValor.builder()
                  .codigo(beneficiary.getCodigoSexo())
                  .descripcion(null)
                  .build())
              .parentesco(ItemValor.builder()
                  .codigo(beneficiary.getCodigoParentesco())
                  .descripcion(null)
                  .build())
              .tipoDocumento(ItemValor.builder()
                  .codigo(beneficiary.getCodigoTipoDocumento())
                  .descripcion(null)
                  .build())
              .numeroDocumento(beneficiary.getNumeroDocumento())
              .fechaNacimiento(beneficiary.getFechaNacimiento())
              .porcentajeBeneficio(beneficiary.getPorcentajeBeneficiario())
              .tutor(tutorDto)
              .build();

        }).collect(Collectors.toList());

    return Mono.just(beneficiaryResponseList);
  }

  @Override
  public Mono<BeneficiaryResponse> saveBeneficiary(BeneficiaryRequest beneficiaryRequest) {

    Beneficiario beneficiary = BeneficiaryMapper.beneficiaryToRequest(beneficiaryRequest);
    Tutor tutor = TutorMapper.tutorToRequest(beneficiaryRequest);

    return getBeneficiaryResponseMono(beneficiary, tutor);
  }

  @Override
  public Mono<BeneficiaryResponse> updateBeneficiary(Integer beneficiaryId, BeneficiaryRequest beneficiaryRequest) {
    Optional<Beneficiario> beneficiaryOptional = beneficiaryRepository.findById(beneficiaryId);
    Optional<Tutor> tutorOptional = tutorRepository.findByIdSolicitud(beneficiaryRequest.getIdBeneficiario());

    Beneficiario beneficiary = null;
    Tutor tutor = null;

    if (beneficiaryOptional.isPresent()) {
      beneficiary = beneficiaryOptional.get();
    }
    if ((tutorOptional.isPresent())) {
      tutor = tutorOptional.get();
    }
    return getBeneficiaryResponseMono(beneficiary, tutor);

  }

  private Mono<BeneficiaryResponse> getBeneficiaryResponseMono(Beneficiario beneficiary, Tutor tutor) {
    Beneficiario beneficiaryAdd = beneficiaryRepository.save(beneficiary);
    Tutor tutorAdd = tutorRepository.save(tutor);

    BeneficiaryResponse beneficiaryResponse = BeneficiaryResponse.builder()
        .nombres(beneficiaryAdd.getNombres())
        .apellidoPaterno(beneficiaryAdd.getApellidoPaterno())
        .tutor(TutorDto.builder()
            .nombres(tutorAdd.getNombres())
            .apellidoPaterno(tutorAdd.getApellidoPaterno())
            .build())
        .build();
    return Mono.just(beneficiaryResponse);
  }

  @Override
  public Mono<BeneficiaryResponse> registrarActualizarBeneficiario(BeneficiaryRequest beneficiaryRequest) {
    return null;
  }

  @Override
  public Mono<String> deleteBeneficiary(Integer beneficiaryId, Integer requestId, boolean deleteTutor) {

    Optional<Beneficiario> beneficiaryOptional = beneficiaryRepository.findByIdBeneficiarioAndIdSolicitud(beneficiaryId, requestId);

    String message;

    if (deleteTutor) {
      tutorRepository.deleteByIdSolicitud(requestId);
    }

    if (beneficiaryOptional.isPresent()) {
      beneficiaryRepository.deleteByIdBeneficiarioAndIdSolicitud(beneficiaryId, requestId);
      message = "Beneficiario Eliminado";
    } else {
      message = "Beneficiario no Eliminado";
    }

    return Mono.just(message);
  }
}
