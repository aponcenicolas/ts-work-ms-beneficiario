package lux.pe.na.business;

import lux.pe.na.expose.request.BeneficiaryRequest;
import lux.pe.na.expose.response.BeneficiaryResponse;
import reactor.core.publisher.Mono;

import java.util.List;

public interface BeneficiaryService {

    Mono<List<BeneficiaryResponse>> getBeneficiary(Integer multipleRequestId);

    Mono<BeneficiaryResponse> saveBeneficiary(BeneficiaryRequest beneficiaryRequest);

    Mono<BeneficiaryResponse> updateBeneficiary(Integer beneficiaryId, BeneficiaryRequest beneficiaryRequest);

    Mono<BeneficiaryResponse> registrarActualizarBeneficiario(BeneficiaryRequest beneficiaryRequest);

    Mono<String> deleteBeneficiary(Integer beneficiaryId, Integer requestId, boolean deleteTutor);
}
