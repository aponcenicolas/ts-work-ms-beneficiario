package lux.pe.na.expose.request;

import lombok.*;
import lux.pe.na.model.dto.ItemValor;
import lux.pe.na.model.dto.TutorDto;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class BeneficiaryRequest implements Serializable {

  private static final long serialVersionUID = 1L;

  private Integer idBeneficiario;
  private Integer idSolicitud;
  private ItemValor tipoBeneficiario;
  private String nombres;
  private String apellidoPaterno;
  private String apellidoMaterno;
  private ItemValor sexo;
  private ItemValor parentesco;
  private ItemValor tipoDocumento;
  private String numeroDocumento;
  private Date fechaNacimiento;
  private Double porcentajeBeneficio;
  private TutorDto tutor;
  private String usuario;
}
