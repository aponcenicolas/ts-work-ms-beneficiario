package lux.pe.na.expose;

import lombok.AllArgsConstructor;
import lux.pe.na.business.BeneficiaryService;
import lux.pe.na.expose.request.BeneficiaryRequest;
import lux.pe.na.expose.response.BeneficiaryResponse;
import lux.pe.na.expose.response.Response;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.util.List;

@RestController
@RequestMapping("/")
@AllArgsConstructor
public class BeneficiaryController {

  private final BeneficiaryService beneficiaryService;

  @GetMapping("/{idsolicitudmultiple}/beneficiarios")
  public Mono<Response> findAll(@PathVariable(name = "idsolicitudmultiple") Integer multipartRequestId) {
    Response response = Response.builder()
        .code(200)
        .status("ok")
        .data(beneficiaryService.getBeneficiary(multipartRequestId))
        .build();
    return Mono.just(response);
  }

  @PostMapping(value = "/{idsolicitud}/beneficiarios")
  public Mono<Response> save(@RequestBody BeneficiaryRequest beneficiaryRequest,
                             @PathVariable(value = "idsolicitud") Integer requestId) {
    beneficiaryRequest.setIdSolicitud(requestId);
    Response response = Response.builder()
        .code(201)
        .status("ok")
        .data(beneficiaryService.saveBeneficiary(beneficiaryRequest))
        .build();
    return Mono.just(response);
  }

  @PutMapping(value = "/{idsolicitud}/beneficiarios/{idbeneficiario}")
  public Mono<Response> update(@PathVariable(value = "idbeneficiario") Integer beneficiaryId,
                               @PathVariable(value = "idsolicitud") Integer requestId,
                               @RequestBody BeneficiaryRequest beneficiaryRequest) {
    beneficiaryRequest.setIdSolicitud(requestId);
    Response response = Response.builder()
        .code(201)
        .status("ok")
        .data(beneficiaryService.updateBeneficiary(beneficiaryId, beneficiaryRequest))
        .build();
    return Mono.just(response);
  }

  @DeleteMapping(value = "/{idsolicitud}/beneficiarios/{idbeneficiario}")
  public Mono<Response> delete(@PathVariable(value = "idbeneficiario") Integer beneficiaryId,
                               @PathVariable(value = "idsolicitud") Integer requestId,
                               @RequestParam boolean deleteTutor) {
    Response response = Response.builder()
        .code(200)
        .status("ok")
        .data(beneficiaryService.deleteBeneficiary(beneficiaryId, requestId, deleteTutor))
        .build();
    return Mono.just(response);
  }

}
