package lux.pe.na.mappers;

import lux.pe.na.expose.request.BeneficiaryRequest;
import lux.pe.na.model.Beneficiario;

public final class BeneficiaryMapper {

  public static Beneficiario beneficiaryToRequest(BeneficiaryRequest request) {
    if (request == null) {
      return null;
    }
    return Beneficiario.builder()
        .idSolicitud(request.getIdSolicitud())
        .numeroBeneficiarioDispositivo(request.getTipoBeneficiario().getCodigo())
        .nombres(request.getNombres())
        .apellidoPaterno(request.getApellidoPaterno())
        .apellidoMaterno(request.getApellidoMaterno())
        .codigoSexo(request.getSexo().getCodigo())
        .codigoParentesco(request.getParentesco().getCodigo())
        .codigoTipoDocumento(request.getTipoDocumento().getCodigo())
        .numeroDocumento(request.getNumeroDocumento())
        .fechaNacimiento(request.getFechaNacimiento())
        .porcentajeBeneficio(request.getPorcentajeBeneficio())
        .usuarioCreacion(request.getUsuario())
        .build();
  }
}
