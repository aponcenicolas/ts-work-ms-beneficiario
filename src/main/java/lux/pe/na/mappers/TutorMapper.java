package lux.pe.na.mappers;

import lux.pe.na.expose.request.BeneficiaryRequest;
import lux.pe.na.model.Tutor;

public final class TutorMapper {

  public static Tutor tutorToRequest(BeneficiaryRequest request) {

    if (request == null) {
      return null;
    }

    return Tutor.builder()
        .idSolicitud(request.getIdSolicitud())
        .nombres(request.getNombres())
        .apellidoPaterno(request.getApellidoPaterno())
        .apellidoMaterno(request.getApellidoMaterno())
        .codigoSexo(request.getSexo().getCodigo())
        .codigoParentesco(request.getParentesco().getCodigo())
        .codigoTipoDocumento(request.getTipoDocumento().getCodigo())
        .numeroDocumento(request.getNumeroDocumento())
        .fechaNacimiento(request.getFechaNacimiento())
        .usuarioCreacion(request.getUsuario())
        .build();
  }
}
