package lux.pe.na.repository;


import lux.pe.na.model.Beneficiario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BeneficiaryRepository extends JpaRepository<Beneficiario, Integer>, CustomBeneficiaryRepository {

  // Ya existe el ByIdBeneficiario.

  void deleteByIdBeneficiarioAndIdSolicitud(Integer beneficiaryId, Integer requestId);

  Optional<Beneficiario> findByIdBeneficiarioAndIdSolicitud(Integer beneficiaryId, Integer requestId);

}
