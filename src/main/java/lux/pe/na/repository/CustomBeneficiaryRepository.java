package lux.pe.na.repository;

import lux.pe.na.model.dto.BeneficiarioList;

import java.util.List;

public interface CustomBeneficiaryRepository {

  List<BeneficiarioList> beneficiaryList(Integer multipleRequestId);


}
