package lux.pe.na.repository;

import lux.pe.na.model.Tutor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TutorRepository extends JpaRepository<Tutor, Integer>, CustomTutorRepository {

  void deleteByIdSolicitud(Integer requestId);

  Optional<Tutor> findByIdSolicitud(Integer requestId);

}
