package lux.pe.na.repository;

import lux.pe.na.model.dto.TutorList;

import java.util.List;

public interface CustomTutorRepository {

  List<TutorList> tutorList(Integer multipleRequestId);
}
