package lux.pe.na.repository.impl;

import lux.pe.na.model.dto.TutorList;
import lux.pe.na.repository.CustomTutorRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.Tuple;
import java.sql.Date;
import java.util.List;
import java.util.stream.Collectors;

public class CustomTutorRepositoryImpl implements CustomTutorRepository {

  @PersistenceContext
  private EntityManager entityManager;

  @Override
  public List<TutorList> tutorList(Integer multipleRequestId) {
    String sql = "lol";
    Query query = entityManager.createNativeQuery(sql, Tuple.class);
    List<Tuple> tutors = query.getResultList();
    return tutors.stream()
        .map(tuple -> TutorList.builder()
            .idTutor(Integer.parseInt(tuple.get("IdTutor").toString()))
            .idSolicitud(Integer.parseInt(tuple.get("IdSolicitud").toString()))
            .nombres(tuple.get("Nombres").toString())
            .apellidoPaterno(tuple.get("ApellidoPaterno").toString())
            .apellidoMaterno(tuple.get("ApellidoMaterno").toString())
            .codigoSexo(Integer.parseInt(tuple.get("CodigoSexo").toString()))
            .codigoParentesco(Integer.parseInt(tuple.get("CodigoParentesco").toString()))
            .codigoTipoDocumento(Integer.parseInt(tuple.get("CodigoTipoDocumento").toString()))
            .numeroDocumento(tuple.get("NumeroDocumento").toString())
            .fechaNacimiento(Date.valueOf(tuple.get("FechaNacimiento").toString()))
            .build())
        .collect(Collectors.toList());
  }
}
