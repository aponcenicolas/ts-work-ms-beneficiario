package lux.pe.na.repository.impl;

import lux.pe.na.model.dto.BeneficiarioList;
import lux.pe.na.repository.CustomBeneficiaryRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.Tuple;
import java.sql.Date;
import java.util.List;
import java.util.stream.Collectors;

public class CustomBeneficiaryRepositoryImpl implements CustomBeneficiaryRepository {

  @PersistenceContext
  private EntityManager entityManager;

  @Override
  public List<BeneficiarioList> beneficiaryList(Integer multipleRequestId) {
    String sql = "lol"; // Pendiente query

    Query query = entityManager.createNativeQuery(sql, Tuple.class);
    List<Tuple> beneficiaries = query.getResultList();
    return beneficiaries.stream()
        .map(tuple -> BeneficiarioList.builder()
            .idBeneficiario(Integer.parseInt(tuple.get("IdBeneficiario").toString()))
            .idSolicitud(Integer.parseInt(tuple.get("IdSolicitud").toString()))
            .numeroBeneficiarioDispositivo(Integer.parseInt(tuple.get("NumeroBeneficiarioDispositivo").toString()))
            .codigoTipoBeneficiario(Integer.parseInt(tuple.get("CodigoTipoBeneficiario").toString()))
            .nombres(tuple.get("Nombres").toString())
            .apellidoPaterno(tuple.get("ApellidoPaterno").toString())
            .apellidoMaterno(tuple.get("ApellidoMaterno").toString())
            .codigoSexo(Integer.parseInt(tuple.get("CodigoSexo").toString()))
            .codigoParentesco(Integer.parseInt(tuple.get("CodigoParentesco").toString()))
            .codigoTipoDocumento(Integer.parseInt(tuple.get("CodigoTipoDocumento").toString()))
            .numeroDocumento(tuple.get("NumeroDocumento").toString())
            .fechaNacimiento(Date.valueOf(tuple.get("FechaNacimiento").toString()))
            .porcentajeBeneficiario(Double.parseDouble(tuple.get("PorcentajeBeneficiario").toString()))
            .build())
        .collect(Collectors.toList());
  }
}
